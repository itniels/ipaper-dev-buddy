# iPaper Dev Buddy
[![Build status](https://appveyor.itniels.com/api/projects/status/rnad318y23qph09p?svg=true)](https://appveyor.itniels.com/project/AppVeyor-ITNiels/ipaper-dev-buddy)

## Description
Internal tool for developers at iPaper

## Supported browsers
* Chrome
* Firefox
* Edge

## Changelog:
### Next Version

### Version 2.0.40 (Current)
* [FEATURE] Toggle CMS indicator animation (on/off)
* [FEATURE] Add buttons to toggle tags in PR edit/create view
* [FEATURE] Click branch name in PR to copy to clipboard
* [FEATURE] Firefox auto updates
* [CLEANUP] Removed MyTestValue
* [BUG] Extranious spaces after tags
* [BUG] Icons were not vertically aligned properly

### Version 2.0.31
* [FEATURE] CMS indicator (dev/staging/prod)
* [FEATURE] Highlight my issues in PR list
* [FEATURE] Dim 'WIP' issues in PR list
* [FEATURE] Color target branch names
* [FEATURE] Pull conflict status for all PRs in list on page load
* [FEATURE] Hides the first row background in PR list that is always selected

### Version 2.0.0
* [FEATURE] Highlights issue numbers
* [FEATURE] Highlight tags
* [FEATURE] Use icons instead of tags
* [FEATURE] Always shows issue number in diff view
* [FEATURE] Merge protection