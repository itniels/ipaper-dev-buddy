﻿const webpack = require("webpack");
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const srcDir = '../src/';

module.exports = {
	entry: {
		// Chrome
		'chrome/popup': path.join(__dirname, srcDir + 'popup.ts'),
		'chrome/options': path.join(__dirname, srcDir + 'options.ts'),
		'chrome/background': path.join(__dirname, srcDir + 'background.ts'),
		'chrome/content': path.join(__dirname, srcDir + 'content.ts'),
		// Edge
		'edge/popup': path.join(__dirname, srcDir + 'popup.ts'),
		'edge/options': path.join(__dirname, srcDir + 'options.ts'),
		'edge/background': path.join(__dirname, srcDir + 'background.ts'),
		'edge/content': path.join(__dirname, srcDir + 'content.ts'),
		// Firefox
		'firefox/popup': path.join(__dirname, srcDir + 'popup.ts'),
		'firefox/options': path.join(__dirname, srcDir + 'options.ts'),
		'firefox/background': path.join(__dirname, srcDir + 'background.ts'),
		'firefox/content': path.join(__dirname, srcDir + 'content.ts')
	},
	output: {
		path: path.join(__dirname, '../dist'),
		filename: '[name].js'
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/
			},
			{
				test: require.resolve('jquery'),
				use: [{
					loader: 'expose-loader',
					options: '$'
				}]
			}
		]
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js']
	},
	plugins: [
		// exclude locale files in moment
		new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
		// Dependencies
		new CopyPlugin([{ from: 'jquery.min.js', to: './chrome/thirdparty' }], { context: 'node_modules/jquery/dist' }),
		new CopyPlugin([{ from: 'jquery.min.js', to: './edge/thirdparty' }], { context: 'node_modules/jquery/dist' }),
		new CopyPlugin([{ from: 'jquery.min.js', to: './firefox/thirdparty' }], { context: 'node_modules/jquery/dist' }),
		// Public
		new CopyPlugin([{ from: '.', to: './chrome' }], { context: 'public' }),
		new CopyPlugin([{ from: '.', to: './edge' }], { context: 'public' }),
		new CopyPlugin([{ from: '.', to: './firefox' }], { context: 'public' }),
		// Manifests
		new CopyPlugin([{ from: '.', to: './chrome' }], { context: 'manifests/chrome_edge' }),
		new CopyPlugin([{ from: '.', to: './edge' }], { context: 'manifests/chrome_edge' }),
		new CopyPlugin([{ from: '.', to: './firefox' }], { context: 'manifests/firefox' })
	]
};