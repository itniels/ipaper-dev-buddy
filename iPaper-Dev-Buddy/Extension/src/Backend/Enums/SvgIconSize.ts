﻿export enum SvgIconSize {
	Small = 'sm',
	Large = 'lg'
}