﻿export enum DomainType {
	Unknown = 'Unknown',
	Development = 'Development',
	Staging = 'Staging',
	Production = 'Production'
}