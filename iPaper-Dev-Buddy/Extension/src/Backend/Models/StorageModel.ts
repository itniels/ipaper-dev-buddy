﻿export interface IStorageData { //eslint-disable-line
	StorageSettingsData: string;
	StorageTogglesData: string;
	StorageCmsData: string;
}

export class StorageSettingsModel {
	// Stash
	public StashAddress: string;
	public CreatePullRequestDescriptionText: string;
}

export class StorageTogglesModel {
	public Active: boolean;
	// Stash
	public StashHighlightIssueNumbers: boolean;
	public StashHighlightTags: boolean;
	public StashUseIcons: boolean;
	public StashMergeProtection: boolean;
	public StashPullConflicts: boolean;
	public StashColorTargetBranch: boolean;
	public StashHighlightMineInList: boolean;
	public StashDimNotReadyInList: boolean;
	public StashTagToggleButtons: boolean;
	public StashHighlightMyLikes: boolean;
	public StashApplySuggestionText: boolean;
	public StashCreatePullRequestDescription: boolean;
	public StashDashboardAutoLoadAllPullRequests: boolean;
	// CMS
	public CmsShowBannerWarning: boolean;
	public CmsAnimateBannerWarning: boolean;
}

export class StorageCmsModel {
	// Domains
	public DevDomains: string[];
	public StagingDomains: string[];
	public ProductionDomains: string[];
}

export class StorageExportModel {
	public settings: StorageSettingsModel;
	public toggles: StorageTogglesModel;
	public cms: StorageCmsModel;
}