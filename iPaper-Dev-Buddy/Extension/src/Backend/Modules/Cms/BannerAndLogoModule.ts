﻿import { AppStorage } from "../../AppStorage";
import { DomainType } from "../../Enums/DomainType";
import { AppSelectors } from "../../AppSelectors";

/**
 * Fixes issue when editing pullrequest by reloading the page
 * @class
 * */
export class CmsBannerAndLogoModule {
	private static CurrentDomainType: DomainType;

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		this.CurrentDomainType = this.GetCurrentDomainType();

		if (this.CurrentDomainType === DomainType.Unknown)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.Toggles.CmsShowBannerWarning)
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		const isLoginPage = $(AppSelectors.Body).hasClass('l-login');
		const isLoginPageSet = $('#ipdb-cms-banner').get().length > 0;
		const isCmsPageSet = $('.c-nav-primary__logo').hasClass('ipdb-cms-logo');

		// Login banner
		if (isLoginPage && !isLoginPageSet) {
			const el = `<div id="ipdb-cms-banner" class="${this.GetBannerClasses(this.CurrentDomainType)}">${this.GetText(this.CurrentDomainType)}</div>`;
			$(el).prependTo('body');
		}

		if (!isLoginPage && !isCmsPageSet)
			$('.c-nav-primary__logo').addClass(this.GetLogoClasses(this.CurrentDomainType));
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		$('#ipdb-cms-banner').remove();
		$('.c-nav-primary__logo').removeClass(this.GetLogoClasses(this.CurrentDomainType))
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Nothing to init
	}

	private static GetCurrentDomainType(): DomainType {
		const domain = document.domain;

		if (AppStorage.Cms.DevDomains.indexOf(domain) >= 0)
			return DomainType.Development;

		if (AppStorage.Cms.StagingDomains.indexOf(domain) >= 0)
			return DomainType.Staging;

		if (AppStorage.Cms.ProductionDomains.indexOf(domain) >= 0)
			return DomainType.Production;

		return DomainType.Unknown;
	}

	private static GetBannerClasses(domainType: DomainType): string {
		switch (domainType) {
			case DomainType.Development:
				return 'ipdb-cms-banner ipdb-cms-banner-development';
			case DomainType.Staging:
				return 'ipdb-cms-banner ipdb-cms-banner-staging';
			case DomainType.Production:
				return 'ipdb-cms-banner ipdb-cms-banner-production';
			default:
		}
	}

	private static GetLogoClasses(domainType: DomainType): string {
		switch (domainType) {
			case DomainType.Development:
				return 'ipdb-cms-logo ipdb-cms-logo-development';
			case DomainType.Staging:
				return 'ipdb-cms-logo ipdb-cms-logo-staging';
			case DomainType.Production:
				return AppStorage.GetCmsAnimateIndicator() ? 'ipdb-cms-logo ipdb-cms-logo-production-animated' : 'ipdb-cms-logo ipdb-cms-logo-production';
			default:
		}
	}

	private static GetText(domainType: DomainType): string {
		switch (domainType) {
			case DomainType.Development:
				return 'DEVELOPMENT';
			case DomainType.Staging:
				return 'STAGING';
			case DomainType.Production:
				return 'PRODUCTION';
			default:
		}
	}
}