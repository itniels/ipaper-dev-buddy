﻿import { AppStorage } from "../../AppStorage";
import { AppConstants } from "../../AppConstants";
import { AppFunctions } from "../../AppFunctions";

/**
 * Highlights all issue numbers on stash pages
 * @class
 * */
export class StashHighlightIssueNumbersModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashHighlightIssueNumbers())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		$('.ipdb-issuenumber').addClass('ipdb-issuenumber-active');
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		$('.ipdb-issuenumber').removeClass('ipdb-issuenumber-active');
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Find elements
		const pullRequestHeader = $('.pull-request-header,.app-header').find('a').get(0);
		const pullRequestListTitles = $('.pull-request-title').not('.field-group,h2');
		const yourWorkPageTitles = $('.title').find('a');

		// Pullrequest Header
		if (pullRequestHeader && !AppFunctions.StringContains(pullRequestHeader.innerHTML, 'ipdb-issuenumber') && AppFunctions.StringContainsRegex(pullRequestHeader.innerHTML, AppConstants.StashRegex_IssueNumber_Html))
			pullRequestHeader.innerHTML = pullRequestHeader.innerHTML.replace(AppConstants.StashRegex_IssueNumber_Html, `<span class="ipdb ipdb-issuenumber">$&</span>`);

		// List Titles
		pullRequestListTitles.each((i, e) => {
			if (!AppFunctions.StringContains(e.innerHTML, 'ipdb-issuenumber') && AppFunctions.StringContainsRegex(e.innerHTML, AppConstants.StashRegex_IssueNumber_Html)) {
				e.innerHTML = e.innerHTML.replace(AppConstants.StashRegex_IssueNumber_Html, `<span class="ipdb ipdb-issuenumber">$&</span>`);
			}
		});

		// Your work titles
		yourWorkPageTitles.each((i, e) => {
			if (!AppFunctions.StringContains(e.innerHTML, 'ipdb-issuenumber') && AppFunctions.StringContainsRegex(e.innerHTML, AppConstants.StashRegex_IssueNumber_Html)) {
				e.innerHTML = e.innerHTML.replace(AppConstants.StashRegex_IssueNumber_Html, `<span class="ipdb ipdb-issuenumber">$&</span>`);
			}
		});
	}
}