﻿import { AppStorage } from "../../AppStorage";
import { AppConstants } from "../../AppConstants";
import { AppFunctions } from "../../AppFunctions";

/**
 * Adds Tag buttons to both create PR and edit PR page/modal
 * @class
 * */
export class StashAddTagButtonsModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashTagToggleButtons())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		const isEditModal = $('[role="dialog"]').find('h4').find('span').get(0)?.innerText === 'Edit Pull Request';
		const isCreatePage = $('.page-panel-content-header').get(0) && $('.page-panel-content-header').get(0).innerHTML === 'Create pull request';
		const hasButtons = $('.pull-request-tags').length > 0;
		const inputFieldSelector = isEditModal ? `#${this.GetModalInputId()}` : '#title';

		if ((isEditModal || isCreatePage) && !hasButtons) {
			const btnEpic = $('<button class="aui-button ipdb-btn-tagtoggle ipdb-btn-tagtoggle-epic" type="button">EPIC</button>');
			const btnHotfix = $('<button class="aui-button ipdb-btn-tagtoggle ipdb-btn-tagtoggle-priority" type="button">HOTFIX</button>');
			const btnWip = $('<button class="aui-button ipdb-btn-tagtoggle ipdb-btn-tagtoggle-wip" type="button">WIP</button>');
			const btnDnm = $('<button class="aui-button ipdb-btn-tagtoggle ipdb-btn-tagtoggle-dnm" type="button">DO NOT MERGE</button>');

			// Add element to dialog
			if (isCreatePage) {
				const fieldGroup = $('<div class="pull-request-tags field-group"></div>');
				const label = $('<label for="title">Toggle tags </label>');
				const btnGroup = $('<div class="aui-buttons"></div>');

				// Assemble element
				label.appendTo($(fieldGroup));
				btnGroup.appendTo($(fieldGroup));
				btnEpic.appendTo($(btnGroup));
				btnHotfix.appendTo($(btnGroup));
				btnWip.appendTo($(btnGroup));
				btnDnm.appendTo($(btnGroup));

				$(fieldGroup).insertAfter('.pull-request-title');
			}
			else if (isEditModal) {
				const fieldGroup = $('<div class="pull-request-tags"></div>');
				const label = $('<label class="pull-request-tags-label" for="pull-request-tag-buttons">Toggle tags</label>');
				const btnGroup = $('<div id="pull-request-tag-buttons" class="aui-buttons"></div>');

				// Assemble element
				label.appendTo($(fieldGroup));
				$('<br/>').appendTo($(fieldGroup));
				btnGroup.appendTo($(fieldGroup));
				btnEpic.appendTo($(btnGroup));
				btnHotfix.appendTo($(btnGroup));
				btnWip.appendTo($(btnGroup));
				btnDnm.appendTo($(btnGroup));

				// Get the parent to the titel group and insert after title
				const titleGroup = $(inputFieldSelector).parent();
				$(fieldGroup).insertAfter(titleGroup);
			}

			// Add event handlers
			$(btnEpic).click(function (e) {
				e.preventDefault();
				if (AppFunctions.ToggleTagInTextInputField(inputFieldSelector, '[EPIC]', AppConstants.StashRegex_Epic))
					$('.ipdb-btn-tagtoggle-epic').addClass('aui-button-primary');
				else
					$('.ipdb-btn-tagtoggle-epic').removeClass('aui-button-primary');
			});

			$(btnHotfix).click(function (e) {
				e.preventDefault();
				if (AppFunctions.ToggleTagInTextInputField(inputFieldSelector, '[HOTFIX]', AppConstants.StashRegex_Priority))
					$('.ipdb-btn-tagtoggle-priority').addClass('aui-button-primary');
				else
					$('.ipdb-btn-tagtoggle-priority').removeClass('aui-button-primary');
			});

			$(btnWip).click(function (e) {
				e.preventDefault();
				if (AppFunctions.ToggleTagInTextInputField(inputFieldSelector, '[WIP]', AppConstants.StashRegex_WorkInProgress))
					$('.ipdb-btn-tagtoggle-wip').addClass('aui-button-primary');
				else
					$('.ipdb-btn-tagtoggle-wip').removeClass('aui-button-primary');
			});

			$(btnDnm).click(function (e) {
				e.preventDefault();
				if (AppFunctions.ToggleTagInTextInputField(inputFieldSelector, '[DO-NOT-MERGE]', AppConstants.StashRegex_DoNotMerge))
					$('.ipdb-btn-tagtoggle-dnm').addClass('aui-button-primary');
				else
					$('.ipdb-btn-tagtoggle-dnm').removeClass('aui-button-primary');
			});
		}

		// pre-load selected
		if (AppFunctions.TextInputFieldContainsTag(inputFieldSelector, AppConstants.StashRegex_Epic))
			$('.ipdb-btn-tagtoggle-epic').addClass('aui-button-primary');
		if (AppFunctions.TextInputFieldContainsTag(inputFieldSelector, AppConstants.StashRegex_Priority))
			$('.ipdb-btn-tagtoggle-priority').addClass('aui-button-primary');
		if (AppFunctions.TextInputFieldContainsTag(inputFieldSelector, AppConstants.StashRegex_WorkInProgress))
			$('.ipdb-btn-tagtoggle-wip').addClass('aui-button-primary');
		if (AppFunctions.TextInputFieldContainsTag(inputFieldSelector, AppConstants.StashRegex_DoNotMerge))
			$('.ipdb-btn-tagtoggle-dnm').addClass('aui-button-primary');
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		$('pull-request-tags').remove();
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Nothing to init
	}

	private static GetModalInputId(): string {
		const input = $('[role="dialog"]').find('[name="title"]');

		if (input)
			return input.attr('id');

		return 'title-uid11';
	}
}