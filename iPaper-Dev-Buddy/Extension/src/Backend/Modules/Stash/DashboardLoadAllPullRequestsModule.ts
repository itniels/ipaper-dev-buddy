﻿import { AppStorage } from "../../AppStorage";

/**
 * Loads all pullrequests on the dashboard instead of having to click "load more"
 * @class
 * */
export class StashDashboardLoadAllPullRequestsModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashDashboardAutoLoadAllPullRequests())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		const btns = $('.more-container').find('button').toArray();

		if (btns.length === 0)
			return;

		btns.forEach((btn) => {
			btn.click();
		});
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		// Nothing to derender
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Nothing to init
	}
}