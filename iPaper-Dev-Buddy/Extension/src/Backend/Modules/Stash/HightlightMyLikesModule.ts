﻿import { AppStorage } from "../../AppStorage";

/**
 * Hightlight a users own likes
 * @class
 * */
export class StashHightlightMyLikesModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashHighlightMyLikes())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		const likeButtons = $('.comment-likes').find('button').toArray();

		likeButtons.forEach((button) => {
			if ($(button).hasClass('liked'))
				$(button.parentElement).addClass('ipdb-highlight-like');
			else
				$(button.parentElement).removeClass('ipdb-highlight-like');
		});
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		const likeButtons = $('.comment-likes').find('.liked').toArray();

		likeButtons.forEach((v) => {
			$(v.parentElement).removeClass('ipdb-highlight-like');
		});
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Nothing to init
	}
}