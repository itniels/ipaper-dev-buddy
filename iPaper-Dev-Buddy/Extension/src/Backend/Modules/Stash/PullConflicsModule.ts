﻿import { AppStorage } from "../../AppStorage";

/**
 * Auto pulls conflicts from API and adds label on pr list reload
 * @class
 * */
export class StashPullConflictsModule {
	private static PulledConflictEndpoints = new Array<string>();

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashPullConflicts())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		const links = $('.pull-request-row, .summary');

		links.each((i, v) => {
			try {
				const domainWithProtoAndPort = window.location.href.split('/').slice(0, 3).join('/');
				const overviewUrl = $(v).find('.pull-request-title, .title').attr('href');

				if (overviewUrl) {
					const newUrl = overviewUrl.replace(domainWithProtoAndPort, '').replace('overview', 'merge');
					const endpoint = `${domainWithProtoAndPort}/rest/api/latest${newUrl}`;

					this.MakeRequest(v, endpoint);
				}
			} catch (e) {
				console.warn(e);
			}
		});
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		// Nothing to derender!
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Nothing to init
	}

	/**
	 * Check if we have a conflict
	 * @method
	 */
	private static MakeRequest(el: HTMLElement, endpoint: string): void {
		if (this.PulledConflictEndpoints.indexOf(endpoint) >= 0)
			return;

		if (window.location.href.indexOf('state=MERGED') >= 0)
			return;

		if (window.location.href.indexOf('state=DECLINED') >= 0)
			return;

		if (window.location.href.indexOf('state=ALL') >= 0)
			return;

		this.PulledConflictEndpoints.push(endpoint);

		// Make request
		$.ajax({
			url: endpoint,
			type: 'GET',
			success: (state) => this.SetLabel(el, state.conflicted),
			error: (err) => console.error('IPDB: ' + err)
		});
	}

	/**
	 * Set the 'Conflicted' label
	 * @method
	 */
	private static SetLabel(el: HTMLElement, conflicted: boolean): void {
		const conflictEl = $(el).find('.conflict');
		const html = '<span class="aui-lozenge aui-lozenge-moved conflicted" title="This pull request has conflicts that need to be resolved before it can be merged.">Conflict!</span>';

		if (conflicted) {
			if (conflictEl.find('span').length === 0)
				conflictEl.add(html);
			else
				conflictEl.find('span').removeClass('aui-lozenge-subtle');
		} else {
			conflictEl.find('span').remove();
		}
	}
}