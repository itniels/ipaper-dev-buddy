﻿import { AppStorage } from "../../AppStorage";

/**
 * Sets the title to the PR title instead of the arbitraty pullrequest number!
 * @class
 * */
export class StashFixPullRequestTitleModule {
	private static HasSetTitle: boolean;

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		if (this.HasSetTitle)
			return;

		const pullRequestHeader = $('.pull-request-header,.app-header').find('h1,h2').get(0);

		if (!pullRequestHeader)
			return;

		this.HasSetTitle = true;

		window.document.title = `${pullRequestHeader.innerText}`;
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		// nothing to derender
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// nothing to init
	}
}