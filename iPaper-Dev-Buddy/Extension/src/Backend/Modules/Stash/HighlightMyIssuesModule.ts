﻿import { AppStorage } from "../../AppStorage";
import { AppFunctions } from "../../AppFunctions";

/**
 * highlights all the current users issues in the lists
 * @class
 * */
export class StashHighlightMyIssuesModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashHighlightMineInList())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		$('.ipdb-summary-row-mine').addClass('ipdb-summary-row-focused');
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		$('.ipdb-summary-row-mine').removeClass('ipdb-summary-row-focused');
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// PR List
		const pullRequestListRows = $('.pull-request-row');
		pullRequestListRows.each((i, htmlElement) => {
			const isMine = $(htmlElement).find('.user-avatar').data('username') === AppFunctions.GetUsernameFromPageData();

			if (isMine)
				$(htmlElement).addClass('ipdb-summary-row-mine');
		});

		// Dashboard
		const pullRequestDashboardListRows = $('.dashboard-pull-requests-table').find('tr');
		pullRequestDashboardListRows.each((i, htmlElement) => {
			const isMine = $(htmlElement).find('.author-avatar').find('span').data('username') === AppFunctions.GetUsernameFromPageData();

			if (isMine)
				$(htmlElement).addClass('ipdb-summary-row-mine');
		});
	}
}