﻿import { AppStorage } from "../../AppStorage";
import { AppConstants } from "../../AppConstants";

/**
 * Removes the merge button on issues that are WIP or DNM
 * @class
 * */
export class StashMergeProtectionModule {
	private static ShouldNotMerge: boolean;

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashMergeProtection())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		if (!this.ShouldNotMerge)
			return;

		$('.merge-button-container').find('.merge-button').addClass('ipdb-btn-merge-disabled hidden').attr('disabled', 'disabled');
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		$('.merge-button-container').find('.merge-button').removeClass('ipdb-btn-merge-disabled hidden').removeAttr('disabled');
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// find if this is a WIP or DNM issue!
		const pullRequestHeader = $('.pull-request-header,.app-header').find('h1,h2').get(0);

		if (!pullRequestHeader)
			return;

		const containsDoNotMerge = AppConstants.StashRegex_DoNotMerge_Html.test(pullRequestHeader.innerHTML);
		const containsWorkInProgress = AppConstants.StashRegex_WorkInProgress_Html.test(pullRequestHeader.innerHTML);

		this.ShouldNotMerge = (containsDoNotMerge || containsWorkInProgress);
	}
}