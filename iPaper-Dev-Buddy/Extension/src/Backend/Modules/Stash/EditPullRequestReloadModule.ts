﻿import { AppStorage } from "../../AppStorage";

/**
 * Fixes issue when editing pullrequest by reloading the page
 * @class
 * */
export class StashEditPullRequestReloadModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		// Add a new click event to reload the page instead of the error
		$('.ipdb-editpullrequest-submit').click(() => {
			// Remove body to hide error
			$('body').addClass('hidden');

			// Only insert once
			if ($('#ipdb-reload-page').get().length < 1)
				$(this.GetReloadPageHtml()).insertBefore($('body'));

			// reload the page as the last thing
			window.setTimeout(() => window.location.reload(), 500);
		});
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		// Nothing to derender!
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Find the first dialog
		const dialog = $("[role='dialog']").get(0);
		// Grab the title
		const headerTitle = $(dialog).find('header').find('span').text();

		// See if we have the right title
		if (!headerTitle || headerTitle.indexOf('Edit Pull Request') < 0)
			return;

		// Add a class so we can find it again
		const submitButton = $(dialog).find('form').find("[type='submit']").get(0);
		$(submitButton).addClass('ipdb-editpullrequest-submit');
	}

	/**
	 * Returns a custom reload page
	 * @method
	 */
	private static GetReloadPageHtml(): string {
		const html = `
<div id="ipdb-reload-page" class="ipdb-reload-page">
	<h1>Please wait while the page saves and reloads!</h1>
	<p>This page is created by iPaper Dev Buddy.</p>
</div>
		`;

		return html;
	}
}