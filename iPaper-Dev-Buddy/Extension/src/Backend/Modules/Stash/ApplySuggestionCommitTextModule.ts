﻿import { AppStorage } from "../../AppStorage";
import { AppFunctions } from "../../AppFunctions";

/**
 * Applies a better default text containing the issue number when applying a comment.
 * @class
 * */
export class StashApplySuggestionCommitTextModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashApplySuggestionText())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		// Get all sections for commiting suggestions
		const dialogs = $('[role="dialog"]').get();

		if (dialogs.length === 0)
			return;

		// Get issue number
		const issueNumberEl = $('.branch-lozenge-content').get(0);

		if (!issueNumberEl)
			return;

		const issueNumber = issueNumberEl.textContent;
		const newtext = `${issueNumber}: Applied PR suggestion`;

		// Loop dialogs
		dialogs.forEach((dialog) => {
			// Make sure we only target the Apply Suggestion modal
			if (AppFunctions.AssertDialogByTitle(dialog, 'Apply suggestion'))
				$(dialog).find('#commit-message-title').val(newtext);
		});
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		// Nothing to derender
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Nothing to init
	}
}