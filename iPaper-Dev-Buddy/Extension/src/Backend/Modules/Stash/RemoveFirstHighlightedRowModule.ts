﻿import { AppStorage } from "../../AppStorage";

/**
 * Removes the styling from the first row in all PR lists!
 * no clue why it's even there!
 * @class
 * */
export class StashRemoveFirstHighlightedRowModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		$('.pull-request-row').removeClass('focused');
		$('tr').removeClass('focused');
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		$('.pull-request-row').addClass('focused');
		$('tr').addClass('focused');
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		$('.pull-request-row').first().addClass('ipdb-summary-row');
	}
}