﻿import { AppStorage } from "../../AppStorage";

/**
 * Dims all WIP issues as they are not ready for review!
 * @class
 * */
export class StashDimNotReadyIssuesModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashDimNotReadyInList())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		$('.ipdb-summary-row-notready').addClass('ipdb-summary-row-dimmed');
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		$('.ipdb-summary-row-notready').removeClass('ipdb-summary-row-dimmed');
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// PR List
		const pullRequestListRows = $('.pull-request-row');

		pullRequestListRows.each((i, htmlElement) => {
			const isDimmed = $(htmlElement).find('.ipdb-workinprogress-tag').length > 0;

			if (isDimmed) {
				$(htmlElement).find('.summary').addClass('ipdb-summary-row-notready');
				$(htmlElement).find('img').addClass('ipdb-summary-row-notready');
			}
		});

		// Dashboard
		const pullRequestDashboardListRows = $('.dashboard-pull-requests-table').find('tr');

		pullRequestDashboardListRows.each((i, htmlElement) => {
			const isDimmed = $(htmlElement).find('.ipdb-workinprogress-tag').length > 0;

			if (isDimmed) {
				$(htmlElement).find('.summary').addClass('ipdb-summary-row-notready');
				$(htmlElement).find('img').addClass('ipdb-summary-row-notready');
			}
		});

	}
}