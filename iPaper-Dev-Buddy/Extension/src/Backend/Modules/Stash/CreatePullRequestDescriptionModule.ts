﻿import { AppStorage } from "../../AppStorage";

/**
 * Sets the description when creating a new PR to a custom defined template in settings.
 * @class
 * */
export class StashCreatePullRequestDescriptionModule {
	private static HasSetText: boolean;

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashCreatePullRequestDescription())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		if (this.HasSetText)
			return;

		const isCreatePrPage = $('.page-panel-content-header').get(0) && $('.page-panel-content-header').get(0).innerHTML === 'Create pull request';

		if (!isCreatePrPage)
			return;

		const descriptionField = $('#pull-request-description');
		const descriptionText = AppStorage.GetStashCreatePullRequestDescriptionText();

		if (!descriptionText || !descriptionField || descriptionField.is(':hidden'))
			return;

		$(descriptionField).val(descriptionText);
		this.HasSetText = true;
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		// Nothing to derender
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Nothing to init
	}
}