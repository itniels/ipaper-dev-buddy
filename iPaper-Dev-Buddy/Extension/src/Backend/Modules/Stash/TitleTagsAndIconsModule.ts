﻿import { AppStorage } from "../../AppStorage";
import { AppConstants } from "../../AppConstants";
import { AppFunctions } from "../../AppFunctions";
import { SvgIconSize } from "../../Enums/SvgIconSize";
import { AppSvgIcons } from "../../AppSvgIcons";

/**
 * Adds sorts and colors the title tags as well as inserts icons depending on settings
 * @class
 * */
export class StashTitleTagsAndIconsModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && (AppStorage.Toggles.StashUseIcons || AppStorage.Toggles.StashHighlightTags))
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		if (AppStorage.GetStashUseIcons()) {
			$('.ipdb-icon').removeClass('hidden');
			$('.ipdb-tag').addClass('hidden');
		} else {
			$('.ipdb-icon').addClass('hidden');
			$('.ipdb-tag').removeClass('hidden');

			// Tag colors
			$('.ipdb-donotmerge-tag').addClass('ipdb-donotmerge-tag-active');
			$('.ipdb-workinprogress-tag').addClass('ipdb-workinprogress-tag-active');
			$('.ipdb-priority-tag').addClass('ipdb-priority-tag-active');
			$('.ipdb-epic-tag').addClass('ipdb-epic-tag-active');
		}
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		$('.ipdb-icon').addClass('hidden');
		$('.ipdb-tag').removeClass('hidden');

		// Tag colors
		$('.ipdb-donotmerge-tag').removeClass('ipdb-donotmerge-tag-active');
		$('.ipdb-workinprogress-tag').removeClass('ipdb-workinprogress-tag-active');
		$('.ipdb-priority-tag').removeClass('ipdb-priority-tag-active');
		$('.ipdb-epic-tag').removeClass('ipdb-epic-tag-active');
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// Find elements
		const pullRequestHeader = $('.pull-request-header,.app-header').find('h1,h2').get(0);
		const pullRequestListTitles = $('.pull-request-title').not('.field-group,h2');
		const yourWorkPageTitles = $('.title').find('a');

		// Pullrequest Header
		if (pullRequestHeader && !AppFunctions.StringContains(pullRequestHeader.innerHTML, 'ipdb-icon'))
			pullRequestHeader.innerHTML = this.AddIconsToHeader(pullRequestHeader.innerHTML, pullRequestHeader.innerText);

		// List Titles
		pullRequestListTitles.each((i, e) => {
			if (!AppFunctions.StringContains(e.innerHTML, 'ipdb-icon')) {
				e.innerHTML = this.AddListTitleTags(e.innerText);
			}
		});

		// 'Your work' titles
		yourWorkPageTitles.each((i, e) => {
			if (!AppFunctions.StringContains(e.innerHTML, 'ipdb-icon')) {
				e.innerHTML = this.AddListTitleTags(e.innerText);
			}
		});
	}

	private static AddIconsToHeader(innerHtml: string, innerText: string): string {
		let newHtml = '';

		// Assert tags in header
		const containsDoNotMerge = AppConstants.StashRegex_DoNotMerge_Html.test(innerText);
		const containsWorkInProgress = AppConstants.StashRegex_WorkInProgress_Html.test(innerText);
		const containsPriority = AppConstants.StashRegex_Priority_Html.test(innerText);
		const containsEpic = AppConstants.StashRegex_Epic_Html.test(innerText);

		// Generate icon html
		const epicIcon = AppSvgIcons.Epic(SvgIconSize.Large, containsEpic, AppStorage.Toggles.StashUseIcons);
		const priorityIcon = AppSvgIcons.Priority(SvgIconSize.Large, containsPriority, AppStorage.Toggles.StashUseIcons);
		const workInProgressIcon = AppSvgIcons.WorkInProgress(SvgIconSize.Large, containsWorkInProgress, AppStorage.Toggles.StashUseIcons);
		const doNotMergeIcon = AppSvgIcons.DoNotMerge(SvgIconSize.Large, containsDoNotMerge, AppStorage.Toggles.StashUseIcons);

		// Add Icon1
		newHtml = containsEpic ? epicIcon : priorityIcon;
		// Add Icon2
		newHtml += workInProgressIcon;
		// Add Icon3
		newHtml += doNotMergeIcon;

		// Add original HTML
		newHtml += this.TagAndSortTags(innerHtml);

		return newHtml;
	}

	private static AddListTitleTags(innerText: string): string {
		let newHtml = '';

		// Assert tags in text
		const containsDoNotMerge = AppConstants.StashRegex_DoNotMerge_Html.test(innerText);
		const containsWorkInProgress = AppConstants.StashRegex_WorkInProgress_Html.test(innerText);
		const containsPriority = AppConstants.StashRegex_Priority_Html.test(innerText);
		const containsEpic = AppConstants.StashRegex_Epic_Html.test(innerText);

		// Generate icon html
		const epicIcon = AppSvgIcons.Epic(SvgIconSize.Small, containsEpic, AppStorage.Toggles.StashUseIcons);
		const priorityIcon = AppSvgIcons.Priority(SvgIconSize.Small, containsPriority, AppStorage.Toggles.StashUseIcons);
		const workInProgressIcon = AppSvgIcons.WorkInProgress(SvgIconSize.Small, containsWorkInProgress, AppStorage.Toggles.StashUseIcons);
		const doNotMergeIcon = AppSvgIcons.DoNotMerge(SvgIconSize.Small, containsDoNotMerge, AppStorage.Toggles.StashUseIcons);

		// Add Icon1
		newHtml = containsEpic ? epicIcon : priorityIcon;
		// Add Icon2
		newHtml += workInProgressIcon;
		// Add Icon3
		newHtml += doNotMergeIcon;

		// Add original HTML
		newHtml += this.TagAndSortTags(innerText);

		return newHtml;
	}

	private static TagAndSortTags(html: string): string {
		const isTextHidden = AppStorage.Toggles.StashUseIcons ? ' hidden' : '';
		let newHtml = '';

		// Extract tags
		const epicTag = html.match(AppConstants.StashRegex_Epic_Html);
		const priorityTag = html.match(AppConstants.StashRegex_Priority_Html);
		const wipTag = html.match(AppConstants.StashRegex_WorkInProgress_Html);
		const dnmTag = html.match(AppConstants.StashRegex_DoNotMerge_Html);

		// Remove from original html
		html = html.replace(AppConstants.StashRegex_Epic_Html, '');
		html = html.replace(AppConstants.StashRegex_Priority_Html, '');
		html = html.replace(AppConstants.StashRegex_WorkInProgress_Html, '');
		html = html.replace(AppConstants.StashRegex_DoNotMerge_Html, '');

		// Add tags back in order (same order as icons)
		if (epicTag && epicTag.length > 0)
			newHtml += `<span class="ipdb ipdb-tag ipdb-epic-tag${isTextHidden}">${epicTag[0].trim().toUpperCase()}&nbsp</span>`;
		if (priorityTag && priorityTag.length > 0)
			newHtml += `<span class="ipdb ipdb-tag ipdb-priority-tag${isTextHidden}">${priorityTag[0].trim().toUpperCase()}&nbsp</span>`;
		if (wipTag && wipTag.length > 0)
			newHtml += `<span class="ipdb ipdb-tag ipdb-workinprogress-tag${isTextHidden}">${wipTag[0].trim().toUpperCase()}&nbsp</span>`;
		if (dnmTag && dnmTag.length > 0)
			newHtml += `<span class="ipdb ipdb-tag ipdb-donotmerge-tag${isTextHidden}">${dnmTag[0].trim().toUpperCase().replace(/ /g, '-')}&nbsp</span>`;

		// Add title back
		newHtml += html.trim();

		return newHtml;
	}
}