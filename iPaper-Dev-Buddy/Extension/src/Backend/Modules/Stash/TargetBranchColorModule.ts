﻿import { AppStorage } from "../../AppStorage";

/**
 * Colors the branche tags
 * @class
 * */
export class StashTargetBranchColorModule {

	/**
	* Renders this module
	* @method
	* */
	public static RenderModule(): void {
		// Validate domain is correct
		if (document.domain !== AppStorage.Settings.StashAddress)
			return;

		// Init page
		if (AppStorage.GetActive())
			this.InitPage();

		// Should we render or de-render module
		if (AppStorage.GetActive() && AppStorage.GetStashColorTargetBranch())
			this.Render();
		else
			this.DeRender();
	}

	/**
	 * Renders the module
	 * @method
	 * */
	private static Render(): void {
		$('.ipdb-branch-tag-master').addClass('ipdb-branch-tag-master-active');
		$('.ipdb-branch-tag-production').addClass('ipdb-branch-tag-production-active');
		$('.ipdb-branch-tag-other').addClass('ipdb-branch-tag-other-active');
	}

	/**
	* De-renders the module
	* @method
	* */
	private static DeRender(): void {
		$('.ipdb-branch-tag-master').removeClass('ipdb-branch-tag-master-active');
		$('.ipdb-branch-tag-production').removeClass('ipdb-branch-tag-production-active');
		$('.ipdb-branch-tag-other').removeClass('ipdb-branch-tag-other-active');
	}

	/**
	* Initializes the pages with tags and classes
	* @method
	* */
	private static InitPage(): void {
		// PR meta data
		//const branchTagsInPullRequest = $('.branch-dest').find('.branch-lozenge');
		//branchTagsInPullRequest.each((i, e) => {
		//	this.TagBranchTag(e);
		//});

		// PR List
		const branchTagsInList = $('.ref-label').find('span');
		branchTagsInList.each((i, e) => {
			this.TagBranchTag(e);
		});
	}

	/**
 * Tags branch classes with nice colors
 */
	private static TagBranchTag(htmlElement: HTMLElement): void {
		const branchName = $(htmlElement).get(0).innerText;

		if (branchName.toLowerCase() === 'master')
			$(htmlElement).addClass('ipdb ipdb-branch-tag-master');
		else if (branchName.toLowerCase() === 'production')
			$(htmlElement).addClass('ipdb ipdb-branch-tag-production');
		else
			$(htmlElement).addClass('ipdb ipdb-branch-tag-other');
	}
}