﻿export class AppConstants {
	public static readonly PageMutationDebounceRate: number = 100

	public static StashRegex_IssueNumber = new RegExp('(IP-\\d+([\\w-]+)?(\\:)?)', 'gi');
	public static StashRegex_DoNotMerge = new RegExp('(\\s?\\[DO(.)?NOT(.)?MERGE\\]\\s?)', 'gi');
	public static StashRegex_WorkInProgress = new RegExp('(\\s?\\[WIP\\]\\s?)', 'gi');
	public static StashRegex_Priority = new RegExp('(\\s?\\[HOT.+?\\]\\s?)', 'gi');
	public static StashRegex_Epic = new RegExp('(\\s?\\[EPIC\\]\\s?)', 'gi');

	public static StashRegex_IssueNumber_Html = new RegExp('(IP-\\d+([\\w-]+)?(\\:)?)' + '(?!</span>)', 'gi');
	public static StashRegex_DoNotMerge_Html = new RegExp('(\\s?\\[DO(.)?NOT(.)?MERGE\\]\\s?)' + '(?!</span>)', 'gi');
	public static StashRegex_WorkInProgress_Html = new RegExp('(\\s?\\[WIP\\]\\s?)' + '(?!</span>)', 'gi');
	public static StashRegex_Priority_Html = new RegExp('(\\s?\\[HOT.+?\\]\\s?)' + '(?!</span>)', 'gi');
	public static StashRegex_Epic_Html = new RegExp('(\\s?\\[EPIC\\]\\s?)' + '(?!</span>)', 'gi');
}