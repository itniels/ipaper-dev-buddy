﻿import { StorageSettingsModel, StorageTogglesModel, StorageCmsModel, IStorageData, StorageExportModel } from "./Models/StorageModel";

export class AppStorage {
	// Models
	public static Settings: StorageSettingsModel = new StorageSettingsModel();
	public static Toggles: StorageTogglesModel = new StorageTogglesModel();
	public static Cms: StorageCmsModel = new StorageCmsModel();
	// ===================================================================================================

	// SETTINGS: Stash_Address
	public static GetStashAddress(): string { return AppStorage.Settings.StashAddress; };
	public static SetStashAddress(value: string): void { AppStorage.Settings.StashAddress = value; this.UpdateChromeStorage(); };

	// SETTINGS: Stash_CreatePrDescription
	public static GetStashCreatePullRequestDescriptionText(): string { return AppStorage.Settings.CreatePullRequestDescriptionText; };
	public static SetStashCreatePullRequestDescriptionText(value: string): void { AppStorage.Settings.CreatePullRequestDescriptionText = value; this.UpdateChromeStorage(); };

	// TOGGLES: Active
	public static GetActive(): boolean { return AppStorage.Toggles.Active; };
	public static SetActive(value: boolean): void { AppStorage.Toggles.Active = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_HighlightIssueNumbers
	public static GetStashHighlightIssueNumbers(): boolean { return AppStorage.Toggles.StashHighlightIssueNumbers; };
	public static SetStashHighlightIssueNumbers(value: boolean): void { AppStorage.Toggles.StashHighlightIssueNumbers = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_HighlightTags
	public static GetStashHighlightTags(): boolean { return AppStorage.Toggles.StashHighlightTags; };
	public static SetStashHighlightTags(value: boolean): void { AppStorage.Toggles.StashHighlightTags = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_UseIcons
	public static GetStashUseIcons(): boolean { return AppStorage.Toggles.StashUseIcons; };
	public static SetStashUseIcons(value: boolean): void { AppStorage.Toggles.StashUseIcons = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_MergeProtection
	public static GetStashMergeProtection(): boolean { return AppStorage.Toggles.StashMergeProtection; };
	public static SetStashMergeProtection(value: boolean): void { AppStorage.Toggles.StashMergeProtection = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_PullConflicts
	public static GetStashPullConflicts(): boolean { return AppStorage.Toggles.StashPullConflicts; };
	public static SetStashPullConflicts(value: boolean): void { AppStorage.Toggles.StashPullConflicts = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_StashColorTargetBranch
	public static GetStashColorTargetBranch(): boolean { return AppStorage.Toggles.StashColorTargetBranch; };
	public static SetStashColorTargetBranch(value: boolean): void { AppStorage.Toggles.StashColorTargetBranch = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_StashHighlightMineInList
	public static GetStashHighlightMineInList(): boolean { return AppStorage.Toggles.StashHighlightMineInList; };
	public static SetStashHighlightMineInList(value: boolean): void { AppStorage.Toggles.StashHighlightMineInList = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_StashTagToggleButtons
	public static GetStashTagToggleButtons(): boolean { return AppStorage.Toggles.StashTagToggleButtons; };
	public static SetStashTagToggleButtons(value: boolean): void { AppStorage.Toggles.StashTagToggleButtons = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_StashTagToggleButtons
	public static GetStashHighlightMyLikes(): boolean { return AppStorage.Toggles.StashHighlightMyLikes; };
	public static SetStashHighlightMyLikes(value: boolean): void { AppStorage.Toggles.StashHighlightMyLikes = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_StashDimNotReadyInList
	public static GetStashDimNotReadyInList(): boolean { return AppStorage.Toggles.StashDimNotReadyInList; };
	public static SetStashDimNotReadyInList(value: boolean): void { AppStorage.Toggles.StashDimNotReadyInList = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_StashDimNotReadyInList
	public static GetStashApplySuggestionText(): boolean { return AppStorage.Toggles.StashApplySuggestionText; };
	public static SetStashApplySuggestionText(value: boolean): void { AppStorage.Toggles.StashApplySuggestionText = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_StashCreatePullRequestDescription
	public static GetStashCreatePullRequestDescription(): boolean { return AppStorage.Toggles.StashCreatePullRequestDescription; };
	public static SetStashCreatePullRequestDescription(value: boolean): void { AppStorage.Toggles.StashCreatePullRequestDescription = value; this.UpdateChromeStorage(); };

	// TOGGLES: Stash_Toggle_StashDashboardAutoLoadAllPullRequests
	public static GetStashDashboardAutoLoadAllPullRequests(): boolean { return AppStorage.Toggles.StashDashboardAutoLoadAllPullRequests; };
	public static SetStashDashboardAutoLoadAllPullRequests(value: boolean): void { AppStorage.Toggles.StashDashboardAutoLoadAllPullRequests = value; this.UpdateChromeStorage(); };

	// TOGGLES: Cms_Toggle_Indicator
	public static GetCmsShowIndicator(): boolean { return AppStorage.Toggles.CmsShowBannerWarning; };
	public static SetCmsShowIndicator(value: boolean): void { AppStorage.Toggles.CmsShowBannerWarning = value; this.UpdateChromeStorage(); };

	// TOGGLES: Cms_Toggle_AnimatedIndicator
	public static GetCmsAnimateIndicator(): boolean { return AppStorage.Toggles.CmsAnimateBannerWarning; };
	public static SetCmsAnimateIndicator(value: boolean): void { AppStorage.Toggles.CmsAnimateBannerWarning = value; this.UpdateChromeStorage(); };

	// CMS: Cms_DevelopmentDomains
	public static GetCmsDevelopmentDomains(): string[] { return AppStorage.Cms.DevDomains; };
	public static SetCmsDevelopmentDomains(value: string[]): void { AppStorage.Cms.DevDomains = value; this.UpdateChromeStorage(); };

	// CMS: Cms_StagingDomains
	public static GetCmsStagingDomains(): string[] { return AppStorage.Cms.StagingDomains; };
	public static SetCmsStagingDomains(value: string[]): void { AppStorage.Cms.StagingDomains = value; this.UpdateChromeStorage(); };

	// CMS: Cms_ProductionDomains
	public static GetCmsProductionDomains(): string[] { return AppStorage.Cms.ProductionDomains; };
	public static SetCmsProductionDomains(value: string[]): void { AppStorage.Cms.ProductionDomains = value; this.UpdateChromeStorage(); };

	/**
	 * Initializes the storage
	 * */
	public static InitStorage(): Promise<any> { //eslint-disable-line
		return new Promise(function (resolve) {
			chrome.storage.sync.get(function (data) {
				const storage = data as IStorageData;

				if (storage.StorageSettingsData)
					AppStorage.Settings = JSON.parse(storage.StorageSettingsData);

				if (storage.StorageTogglesData)
					AppStorage.Toggles = JSON.parse(storage.StorageTogglesData);

				if (storage.StorageCmsData)
					AppStorage.Cms = JSON.parse(storage.StorageCmsData);

				resolve();
			});
		});
	}

	/**
	 * Listens for changes to storage values and updates the local storage and runs an optional callback
	 * @param callback
	 */
	public static ListenForChanges(callback: any = null): void { //eslint-disable-line
		chrome.storage.onChanged.addListener(function () {
			AppStorage.InitStorage().then(() => {
				if (callback)
					callback();
			});
		});
	}

	/**
	 * Imports settings and saves them to Chrome sync storage
	 * @param json
	 */
	public static ImportSettings(json: string): void {
		const imports = JSON.parse(json) as StorageExportModel;

		AppStorage.Settings = imports.settings;
		AppStorage.Toggles = imports.toggles;
		AppStorage.Cms = imports.cms;

		this.UpdateChromeStorage();
	}

	/**
	 * Export all settings as json string
	 * */
	public static ExportSettings(): string {
		const exports = new StorageExportModel();
		exports.settings = AppStorage.Settings;
		exports.toggles = AppStorage.Toggles;
		exports.cms = AppStorage.Cms;

		return JSON.stringify(exports);
	}

	/**
	 * Updates the chrome sync storage
	 * */
	private static UpdateChromeStorage(): void {
		const jsonSettings = JSON.stringify(this.Settings);
		const jsonToggles = JSON.stringify(this.Toggles);
		const jsonCms = JSON.stringify(this.Cms);

		chrome.storage.sync.set({
			StorageSettingsData: jsonSettings,
			StorageTogglesData: jsonToggles,
			StorageCmsData: jsonCms
		});
	}
}