﻿export class AppFunctions {

	/**
	 * Retrieves the app version from manifest file
	 * @method
	 */
	public static GetVersion(): string {
		return chrome.runtime.getManifest().version;
	}

	/**
	 * Sets the app version from manifest file
	 * @method
	 */
	public static SetVersionInHtml(): void {
		$('.app-version').text(this.GetVersion());
	}

	/**
	 * Sets the buttons color state to green for enabled or defaults to red
	 * @method
	 */
	public static SetButtonState(buttonId: string, enabled: boolean): void {
		const btn = $('#' + buttonId);

		if (enabled)
			btn.addClass('green');
		else
			btn.removeClass('green');
	}

	/**
	 * Toggles a buttons disabled state
	 * @method
	 */
	public static SetButtonDisabled(buttonId: string, disabled: boolean): void {
		const btn = $('#' + buttonId);

		if (disabled)
			btn.attr('disabled', 'disabled');
		else
			btn.removeAttr('disabled');
	}

	/**
	 * Setst he text of a button
	 * @method
	 */
	public static SetButtonText(buttonId: string, text: string): void {
		$('#' + buttonId).val(text);
	}

	/**
	 * Returns the value of a text field
	 * @method
	 */
	public static GetTextField(fieldId: string): string {
		const value = $('#' + fieldId).val();

		if (!value)
			return null;

		return value.toString();
	}

	/**
	 * Sets a textfields value
	 * @method
	 */
	public static SetTextField(fieldId: string, value: string): void {
		$('#' + fieldId).val(value);
	}

	/**
	 * Sets the text of the users clipboard
	 * @method
	 */
	public static SetClipboard(text: string): void {
		document.addEventListener('copy', (e: ClipboardEvent) => {
			e.clipboardData.setData('text/plain', text);
			e.preventDefault();
			document.removeEventListener('copy', null);
		});
		document.execCommand('copy');
	}

	/**
	 * HTML encodes a string
	 * @method
	 */
	public static HtmlEncodeString(value: string): string {
		return String(value)
			.replace(/&/g, '&amp;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;')
			.replace(/"/g, '&quot;');
	}

	/**
	 * Check in input fields for a value
	 * @method
	 */
	public static TextInputFieldContainsTag(selector: string, tagRegex: RegExp): boolean {
		const text = $(selector).val();

		if (!text)
			return false;

		return tagRegex.test(text.toString());
	}

	/**
	 * Updates the value of an input field.
	 * This works with react fields as well now
	 * Due to react we need to dispatch an input event to make it pick up the changes in it's observable
	 * @method
	 */
	public static ToggleTagInTextInputField(selector: string, tag: string, tagRegex: RegExp): boolean {
		const text = $(selector).val();
		const containsTag = this.TestTextForReactInputString(text.toString(), tagRegex, 4);
		const event = new Event('input', { bubbles: true, cancelable: true });
		const el = $(selector).get(0);

		if (containsTag) {
			const newText = text.toString().replace(tagRegex, ' ').trim();
			$(el).val(newText);
			el.dispatchEvent(event);

			return false;
		}
		else {
			const newText = `${tag} ${text}`.trim();
			$(el).val(newText);
			el.dispatchEvent(event);

			return true;
		}
	}

	/**
	 * Searches a string for a regex
	 * @method
	 */
	public static StringContainsRegex(str: string, regex: RegExp): boolean {
		return regex.test(str);
	}

	/**
	 * Searches a string for a string
	 * @method
	 */
	public static StringContains(str: string, searchString: string): boolean {
		return str.indexOf(searchString) > -1;
	}

	/**
	 * Returns the current username
	 * @method
	 * */
	public static GetUsernameFromPageData(): string {
		const username = $('#current-user').data('username');

		return username;
	}

	/**
	 * Matches a dialog with a title.
	 * Since BitBucket use the same dialog for multiple purposes, we have to go by the title
	 * @method
	 */
	public static AssertDialogByTitle(dialog: HTMLElement, title: string) {
		return $(dialog).find('h4').find('span').text().indexOf(title) >= 0;
	}

	/**
	 * Workaround for some inputs where react messes with the read value!
	 * Sometimes you will get 'false, true, false, true' == true!
	 * @method
	 */
	private static TestTextForReactInputString(input: string, regex: RegExp, iterations: number) {
		for (let i = 0; i < iterations; i++) {
			if (regex.test(input))
				return true;
		}

		return false;
	}
}