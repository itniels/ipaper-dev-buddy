﻿import { SvgIconSize } from "./Enums/SvgIconSize";

export class AppSvgIcons {
	public static Priority(size: SvgIconSize, active: boolean, visible: boolean): string {
		const iconClasses = visible ? 'ipdb ipdb-icon' : 'ipdb ipdb-icon hidden';
		const activeClass = active ? 'ipdb-icon-priority-active' : 'ipdb-icon-priority';
		const sizeClass = `ipdb-icon-${size.toString()}`;

		return `<span title="HOTFIX"><svg xmlns="http://www.w3.org/2000/svg" class="${iconClasses} ${activeClass} ${sizeClass}" viewBox="0 0 384 512"><path d="M216 23.858c0-23.802-30.653-32.765-44.149-13.038C48 191.851 224 200 224 288c0 35.629-29.114 64.458-64.85 63.994C123.98 351.538 96 322.22 96 287.046v-85.51c0-21.703-26.471-32.225-41.432-16.504C27.801 213.158 0 261.332 0 320c0 105.869 86.131 192 192 192s192-86.131 192-192c0-170.29-168-193.003-168-296.142z"/></svg></span>`;
	}

	public static DoNotMerge(size: SvgIconSize, active: boolean, visible: boolean): string {
		const iconClasses = visible ? 'ipdb ipdb-icon' : 'ipdb ipdb-icon hidden';
		const activeClass = active ? 'ipdb-icon-donotmerge-active' : 'ipdb-icon-donotmerge';
		const sizeClass = `ipdb-icon-${size.toString()}`;

		return `<span title="DO NOT MERGE"><svg xmlns="http://www.w3.org/2000/svg" class="${iconClasses} ${activeClass} ${sizeClass}" viewBox="0 0 512 512"><path d="M256 8C119.034 8 8 119.033 8 256s111.034 248 248 248 248-111.034 248-248S392.967 8 256 8zm130.108 117.892c65.448 65.448 70 165.481 20.677 235.637L150.47 105.216c70.204-49.356 170.226-44.735 235.638 20.676zM125.892 386.108c-65.448-65.448-70-165.481-20.677-235.637L361.53 406.784c-70.203 49.356-170.226 44.736-235.638-20.676z"/></svg></span>`;
	}

	public static WorkInProgress(size: SvgIconSize, active: boolean, visible: boolean): string {
		const iconClasses = visible ? 'ipdb ipdb-icon' : 'ipdb ipdb-icon hidden';
		const activeClass = active ? 'ipdb-icon-workinprogress-active' : 'ipdb-icon-workinprogress';
		const sizeClass = `ipdb-icon-${size.toString()}`;

		return `<span title="WORK IN PROGRESS"><svg xmlns="http://www.w3.org/2000/svg" class="${iconClasses} ${activeClass} ${sizeClass}" viewBox="0 0 512 512"><path d="M507.73 109.1c-2.24-9.03-13.54-12.09-20.12-5.51l-74.36 74.36-67.88-11.31-11.31-67.88 74.36-74.36c6.62-6.62 3.43-17.9-5.66-20.16-47.38-11.74-99.55.91-136.58 37.93-39.64 39.64-50.55 97.1-34.05 147.2L18.74 402.76c-24.99 24.99-24.99 65.51 0 90.5 24.99 24.99 65.51 24.99 90.5 0l213.21-213.21c50.12 16.71 107.47 5.68 147.37-34.22 37.07-37.07 49.7-89.32 37.91-136.73zM64 472c-13.25 0-24-10.75-24-24 0-13.26 10.75-24 24-24s24 10.74 24 24c0 13.25-10.75 24-24 24z" /></svg></span>`;
	}

	public static Epic(size: SvgIconSize, active: boolean, visible: boolean): string {
		const iconClasses = visible ? 'ipdb ipdb-icon' : 'ipdb ipdb-icon hidden';
		const activeClass = active ? 'ipdb-icon-epic-active' : 'ipdb-icon-epic';
		const sizeClass = `ipdb-icon-${size.toString()}`;

		return `<span title="EPIC"><svg xmlns="http://www.w3.org/2000/svg" class="${iconClasses} ${activeClass} ${sizeClass}" viewBox="0 0 512 512"><path fill-rule="evenodd" d="M451.047619,243.809524 L414.47619,243.809524 L414.47619,146.285714 C414.47619,119.466667 392.533333,97.5238095 365.714286,97.5238095 L268.190476,97.5238095 L268.190476,60.952381 C268.190476,27.3066667 240.88381,0 207.238095,0 C173.592381,0 146.285714,27.3066667 146.285714,60.952381 L146.285714,97.5238095 L48.7619048,97.5238095 C21.9428571,97.5238095 0.243809524,119.466667 0.243809524,146.285714 L0.243809524,238.933333 L36.5714286,238.933333 C72.8990476,238.933333 102.4,268.434286 102.4,304.761905 C102.4,341.089524 72.8990476,370.590476 36.5714286,370.590476 L0,370.590476 L0,463.238095 C0,490.057143 21.9428571,512 48.7619048,512 L141.409524,512 L141.409524,475.428571 C141.409524,439.100952 170.910476,409.6 207.238095,409.6 C243.565714,409.6 273.066667,439.100952 273.066667,475.428571 L273.066667,512 L365.714286,512 C392.533333,512 414.47619,490.057143 414.47619,463.238095 L414.47619,365.714286 L451.047619,365.714286 C484.693333,365.714286 512,338.407619 512,304.761905 C512,271.11619 484.693333,243.809524 451.047619,243.809524 Z"/></svg></span>`
	}
}