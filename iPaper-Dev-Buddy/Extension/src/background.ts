﻿/**
 * Contains logic for installation and load
 */

import { AppStorage } from "./Backend/AppStorage";
import { AppFunctions } from "./Backend/AppFunctions";

// On install: Set default values in storage
chrome.runtime.onInstalled.addListener(function () {
	console.log('Version: ' + AppFunctions.GetVersion());

	console.log('Initializing storage');
	//chrome.storage.sync.clear();	// Debug clear storage

	AppStorage.InitStorage().then(() => {
		console.log('Installing: Setting default values if missing');

		// Settings
		if (AppStorage.GetStashAddress() === void (0)) { AppStorage.SetStashAddress('stash.mydomain.com'); }

		// Toggles
		if (AppStorage.GetActive() === void(0)) { AppStorage.SetActive(true); }
		if (AppStorage.GetStashHighlightIssueNumbers() === void (0)) { AppStorage.SetStashHighlightIssueNumbers(true); }
		if (AppStorage.GetStashHighlightTags() === void (0)) { AppStorage.SetStashHighlightTags(true); }
		if (AppStorage.GetStashUseIcons() === void (0)) { AppStorage.SetStashUseIcons(true); }
		if (AppStorage.GetStashMergeProtection() === void (0)) { AppStorage.SetStashMergeProtection(true); }
		if (AppStorage.GetStashPullConflicts() === void (0)) { AppStorage.SetStashPullConflicts(true); }
		if (AppStorage.GetStashColorTargetBranch() === void (0)) { AppStorage.SetStashColorTargetBranch(true); }
		if (AppStorage.GetStashHighlightMineInList() === void (0)) { AppStorage.SetStashHighlightMineInList(true); }
		if (AppStorage.GetStashDimNotReadyInList() === void (0)) { AppStorage.SetStashDimNotReadyInList(true); }
		if (AppStorage.GetStashTagToggleButtons() === void (0)) { AppStorage.SetStashTagToggleButtons(true); }
		if (AppStorage.GetStashHighlightMyLikes() === void (0)) { AppStorage.SetStashHighlightMyLikes(true); }
		if (AppStorage.GetStashApplySuggestionText() === void (0)) { AppStorage.SetStashApplySuggestionText(true); }
		if (AppStorage.GetStashCreatePullRequestDescription() === void (0)) { AppStorage.SetStashCreatePullRequestDescription(true); }
		if (AppStorage.GetStashDashboardAutoLoadAllPullRequests() === void (0)) { AppStorage.SetStashDashboardAutoLoadAllPullRequests(true); }

		if (AppStorage.GetCmsShowIndicator() === void (0)) { AppStorage.SetCmsShowIndicator(true); }
		if (AppStorage.GetCmsAnimateIndicator() === void (0)) { AppStorage.SetCmsAnimateIndicator(true); }

		// CMS
		if (AppStorage.GetCmsDevelopmentDomains() === void (0)) { AppStorage.SetCmsDevelopmentDomains(new Array<string>()); }
		if (AppStorage.GetCmsStagingDomains() === void (0)) { AppStorage.SetCmsStagingDomains(new Array<string>()); }
		if (AppStorage.GetCmsProductionDomains() === void (0)) { AppStorage.SetCmsProductionDomains(new Array<string>()); }

		// Debug output
		//console.log('Settings', AppStorage.Settings);
		//console.log('Toggles', AppStorage.Toggles);
		//console.log('CMS', AppStorage.Cms);

		console.log('Installing: Finished Setting default values');
	});

	console.log('Installing: Completed');
});