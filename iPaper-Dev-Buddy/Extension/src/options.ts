﻿/**
 * Contains logic for the options page.
 */
import { AppFunctions } from "./Backend/AppFunctions";
import { AppStorage } from "./Backend/AppStorage";

export class OptionsPage {
	/** @method */
	public static OnShow(): void {
		OptionsPage.PopulateFields();
	}

	/** @method */
	public static PopulateFields(): void {
		AppFunctions.SetTextField('input-stash-address', AppStorage.GetStashAddress());
		AppFunctions.SetTextField('input-cms-devdomains', AppStorage.Cms.DevDomains.join(";"));
		AppFunctions.SetTextField('input-cms-stagingdomains', AppStorage.Cms.StagingDomains.join(";"));
		AppFunctions.SetTextField('input-cms-productiondomains', AppStorage.Cms.ProductionDomains.join(";"));
		AppFunctions.SetTextField('input-stash-createprdescription', AppStorage.GetStashCreatePullRequestDescriptionText());
	}

	/** @method */
	public static Save(): void {
		// Disable save button
		AppFunctions.SetButtonDisabled('btn-save-top', true);
		AppFunctions.SetButtonText('btn-save-top', 'Please wait saving...');

		AppFunctions.SetButtonDisabled('btn-save-bottom', true);
		AppFunctions.SetButtonText('btn-save-bottom', 'Please wait saving...');

		// Set all values
		const stashAddress = AppFunctions.GetTextField('input-stash-address');
		const stashCreatePrDescriptionText = AppFunctions.GetTextField('input-stash-createprdescription');
		const cmsDevDomains = AppFunctions.GetTextField('input-cms-devdomains');
		const cmsStagingDomains = AppFunctions.GetTextField('input-cms-stagingdomains');
		const cmsProductionDomains = AppFunctions.GetTextField('input-cms-productiondomains');

		if (stashAddress && stashAddress.length > 0)
			AppStorage.SetStashAddress(stashAddress);
		else
			AppStorage.SetStashAddress('');

		if (stashCreatePrDescriptionText && stashCreatePrDescriptionText.length > 0)
			AppStorage.SetStashCreatePullRequestDescriptionText(stashCreatePrDescriptionText);
		else
			AppStorage.SetStashCreatePullRequestDescriptionText('');

		if (cmsDevDomains && cmsDevDomains.length > 0)
			AppStorage.SetCmsDevelopmentDomains(cmsDevDomains.split(';'));
		else
			AppStorage.SetCmsDevelopmentDomains(['']);

		if (cmsStagingDomains && cmsStagingDomains.length > 0)
			AppStorage.SetCmsStagingDomains(cmsStagingDomains.split(';'));
		else
			AppStorage.SetCmsStagingDomains(['']);

		if (cmsProductionDomains && cmsProductionDomains.length > 0)
			AppStorage.SetCmsProductionDomains(cmsProductionDomains.split(';'));
		else
			AppStorage.SetCmsProductionDomains(['']);

		// Wait and reload the extension
		window.setTimeout(() => { chrome.runtime.reload(); }, 1000);
	}

	/** @method */
	public static ImportSettings(): void {
		const imported = AppFunctions.GetTextField('input-settings-import');
		if (imported.length === 0) {
			alert('Nothing to import!');
			return;
		}

		AppStorage.ImportSettings(imported);
		OptionsPage.PopulateFields();
	}

	/** @method */
	public static ExportSettings(): void {
		const exported = AppStorage.ExportSettings();
		AppFunctions.SetClipboard(exported);
		AppFunctions.SetButtonText('btn-settings-export', 'copied to clipboard');

		window.setTimeout(() => {
			AppFunctions.SetButtonText('btn-settings-export', 'export to clipboard');
		}, 3000);
	}
}

AppFunctions.SetVersionInHtml();
AppStorage.InitStorage().then(() => OptionsPage.OnShow());

// Add handlers to buttons on load
$(document).ready(function () {
	$('#btn-save-top').click(() => OptionsPage.Save());
	$('#btn-save-bottom').click(() => OptionsPage.Save());
	$('#btn-settings-import').click(() => OptionsPage.ImportSettings());
	$('#btn-settings-export').click(() => OptionsPage.ExportSettings());
});