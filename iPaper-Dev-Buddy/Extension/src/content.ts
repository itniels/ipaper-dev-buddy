﻿/**
 * Contains logic that will be injected into pages
 */
import { AppConstants } from "./Backend/AppConstants";
import { AppStorage } from "./Backend/AppStorage";
import { StashHighlightIssueNumbersModule } from "./Backend/Modules/Stash/HighlightIssueNumbersModule";
import { StashTitleTagsAndIconsModule } from "./Backend/Modules/Stash/TitleTagsAndIconsModule";
import { StashEditPullRequestReloadModule } from "./Backend/Modules/Stash/EditPullRequestReloadModule";
import { StashPullConflictsModule } from "./Backend/Modules/Stash/PullConflicsModule";
import { StashHighlightMyIssuesModule } from "./Backend/Modules/Stash/HighlightMyIssuesModule";
import { StashDimNotReadyIssuesModule } from "./Backend/Modules/Stash/DimNotReadyIssuesModule";
import { StashRemoveFirstHighlightedRowModule } from "./Backend/Modules/Stash/RemoveFirstHighlightedRowModule";
import { StashTargetBranchColorModule } from "./Backend/Modules/Stash/TargetBranchColorModule";
import { StashMergeProtectionModule } from "./Backend/Modules/Stash/MergeProtectionModule";
import { StashFixPullRequestTitleModule } from "./Backend/Modules/Stash/FixPullRequestTitleModule";
import { StashApplySuggestionCommitTextModule } from "./Backend/Modules/Stash/ApplySuggestionCommitTextModule";
import { StashAddTagButtonsModule } from "./Backend/Modules/Stash/AddTagButtonsModule";
import { StashHightlightMyLikesModule } from "./Backend/Modules/Stash/HightlightMyLikesModule";
import { CmsBannerAndLogoModule } from "./Backend/Modules/Cms/BannerAndLogoModule";
import { StashCreatePullRequestDescriptionModule } from "./Backend/Modules/Stash/CreatePullRequestDescriptionModule";
import { StashDashboardLoadAllPullRequestsModule } from "./Backend/Modules/Stash/DashboardLoadAllPullRequestsModule";

/** Renders all the modules */
function RenderModules(): void {
	// CMS modules
	CmsBannerAndLogoModule.RenderModule();

	// Stash modules
	StashRemoveFirstHighlightedRowModule.RenderModule();
	StashHighlightIssueNumbersModule.RenderModule();
	StashTitleTagsAndIconsModule.RenderModule();
	StashHighlightMyIssuesModule.RenderModule();
	StashDimNotReadyIssuesModule.RenderModule();
	StashTargetBranchColorModule.RenderModule();
	StashMergeProtectionModule.RenderModule();
	StashFixPullRequestTitleModule.RenderModule();
	StashHightlightMyLikesModule.RenderModule();
	StashAddTagButtonsModule.RenderModule();
	StashApplySuggestionCommitTextModule.RenderModule();
	StashPullConflictsModule.RenderModule();
	StashEditPullRequestReloadModule.RenderModule();
	StashCreatePullRequestDescriptionModule.RenderModule();
	StashDashboardLoadAllPullRequestsModule.RenderModule();
}

/** Listen for DOM changes but limit to 100ms debounce to not go nuts! */
const mutationObs = new MutationObserver(function () {
	// If we are waiting
	if (this.debounce != null)	//eslint-disable-line
		return;

	// Run methods and clear debounce in the future
	this.debounce = window.setTimeout(() => {
		RenderModules();
		this.debounce = null;
	}, AppConstants.PageMutationDebounceRate)
});

AppStorage.InitStorage().then(() => {
	RenderModules();
	mutationObs.observe(document.body, { childList: true, subtree: true });
});

// If settings change we want to run renderes again
AppStorage.ListenForChanges(() => RenderModules());