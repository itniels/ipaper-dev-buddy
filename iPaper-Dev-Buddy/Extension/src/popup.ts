﻿/**
 * Contains logic for the icon popup menu when clicking it.
 */
import { AppFunctions } from "./Backend/AppFunctions";
import { AppStorage } from "./Backend/AppStorage";

export class Popup {

	/** @method */
	public static OnShow(): void {
		Popup.InitButtonStates();
	}

	/** @method */
	public static OpenSettings(): void {
		chrome.runtime.openOptionsPage();
	}

	/** @method */
	public static ToggleActive(buttonId: string): void {
		const value = AppStorage.GetActive();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetActive(!value);
	}

	/** @method */
	public static ToggleStashHighlightIssueNumbers(buttonId: string): void {
		const value = AppStorage.GetStashHighlightIssueNumbers();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashHighlightIssueNumbers(!value);
	}

	/** @method */
	public static ToggleStashHighlightTags(buttonId: string): void {
		const value = AppStorage.GetStashHighlightTags();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashHighlightTags(!value);
	}

	/** @method */
	public static ToggleStashUseIcons(buttonId: string): void {
		const value = AppStorage.GetStashUseIcons();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashUseIcons(!value);
	}

	/** @method */
	public static ToggleStashMergeProtection(buttonId: string): void {
		const value = AppStorage.GetStashMergeProtection();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashMergeProtection(!value);
	}

	/** @method */
	public static ToggleStashPullConflicts(buttonId: string): void {
		const value = AppStorage.GetStashPullConflicts();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashPullConflicts(!value);
	}

	/** @method */
	public static ToggleStashHighlightMineInList(buttonId: string): void {
		const value = AppStorage.GetStashHighlightMineInList();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashHighlightMineInList(!value);
	}

	/** @method */
	public static ToggleStashDimNotReady(buttonId: string): void {
		const value = AppStorage.GetStashDimNotReadyInList();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashDimNotReadyInList(!value);
	}

	/** @method */
	public static ToggleStashColorTargetBranch(buttonId: string): void {
		const value = AppStorage.GetStashColorTargetBranch();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashColorTargetBranch(!value);
	}

	/** @method */
	public static ToggleTagToggleButtons(buttonId: string): void {
		const value = AppStorage.GetStashTagToggleButtons();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashTagToggleButtons(!value);
	}

	/** @method */
	public static ToggleHighlightMyLikes(buttonId: string): void {
		const value = AppStorage.GetStashHighlightMyLikes();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashHighlightMyLikes(!value);
	}

	/** @method */
	public static ToggleApplySuggestionText(buttonId: string): void {
		const value = AppStorage.GetStashApplySuggestionText();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashApplySuggestionText(!value);
	}

	/** @method */
	public static ToggleCreatePullRequestDescription(buttonId: string): void {
		const value = AppStorage.GetStashCreatePullRequestDescription();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashCreatePullRequestDescription(!value);
	}

	/** @method */
	public static ToggleDashboardAutoLoadAllPullRequests(buttonId: string): void {
		const value = AppStorage.GetStashDashboardAutoLoadAllPullRequests();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetStashDashboardAutoLoadAllPullRequests(!value);
	}

	/** @method */
	public static ToggleCmsIndicator(buttonId: string): void {
		const value = AppStorage.GetCmsShowIndicator();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetCmsShowIndicator(!value);
	}

	/** @method */
	public static ToggleCmsAnimatedIndicator(buttonId: string): void {
		const value = AppStorage.GetCmsAnimateIndicator();

		AppFunctions.SetButtonState(buttonId, !value);
		AppStorage.SetCmsAnimateIndicator(!value);
	}

	/** @method */
	public static InitButtonStates(): void {
		AppFunctions.SetButtonState('btn-active', AppStorage.GetActive());
		// Stash
		AppFunctions.SetButtonState('btn-stash-issueNumbers', AppStorage.GetStashHighlightIssueNumbers());
		AppFunctions.SetButtonState('btn-stash-icons', AppStorage.GetStashUseIcons());
		AppFunctions.SetButtonState('btn-stash-tags', AppStorage.GetStashHighlightTags());
		AppFunctions.SetButtonState('btn-stash-merge', AppStorage.GetStashMergeProtection());
		AppFunctions.SetButtonState('btn-stash-checkConflicts', AppStorage.GetStashPullConflicts());
		AppFunctions.SetButtonState('btn-stash-colortargetbranch', AppStorage.GetStashColorTargetBranch());
		AppFunctions.SetButtonState('btn-stash-highlightmineinlist', AppStorage.GetStashHighlightMineInList());
		AppFunctions.SetButtonState('btn-stash-dimneedsworkinlist', AppStorage.GetStashDimNotReadyInList());
		AppFunctions.SetButtonState('btn-stash-tag-toggle-buttons', AppStorage.GetStashTagToggleButtons());
		AppFunctions.SetButtonState('btn-stash-highlight-my-likes', AppStorage.GetStashHighlightMyLikes());
		AppFunctions.SetButtonState('btn-stash-apply-suggestion-text-indicator', AppStorage.GetStashApplySuggestionText());
		AppFunctions.SetButtonState('btn-stash-create-pr-description-indicator', AppStorage.GetStashCreatePullRequestDescription());
		AppFunctions.SetButtonState('btn-stash-dashboard-load-all-pullrequests', AppStorage.GetStashDashboardAutoLoadAllPullRequests());
		// CMS
		AppFunctions.SetButtonState('btn-cms-indicator', AppStorage.GetCmsShowIndicator());
		AppFunctions.SetButtonState('btn-cms-animated-indicator', AppStorage.GetCmsAnimateIndicator());
	}
}

// Set version
AppFunctions.SetVersionInHtml();
AppStorage.InitStorage().then(() => { Popup.OnShow(); });

// Add handlers to buttons on load
$(document).ready(function () {
	$('#btn-active').click(() => Popup.ToggleActive('btn-active'));

	// Stash
	$('#btn-stash-issueNumbers').click(() => Popup.ToggleStashHighlightIssueNumbers('btn-stash-issueNumbers'));
	$('#btn-stash-icons').click(() => Popup.ToggleStashUseIcons('btn-stash-icons'));
	$('#btn-stash-tags').click(() => Popup.ToggleStashHighlightTags('btn-stash-tags'));
	$('#btn-stash-merge').click(() => Popup.ToggleStashMergeProtection('btn-stash-merge'));
	$('#btn-stash-checkConflicts').click(() => Popup.ToggleStashPullConflicts('btn-stash-checkConflicts'));
	$('#btn-stash-colortargetbranch').click(() => Popup.ToggleStashColorTargetBranch('btn-stash-colortargetbranch'));
	$('#btn-stash-highlightmineinlist').click(() => Popup.ToggleStashHighlightMineInList('btn-stash-highlightmineinlist'));
	$('#btn-stash-dimneedsworkinlist').click(() => Popup.ToggleStashDimNotReady('btn-stash-dimneedsworkinlist'));
	$('#btn-stash-tag-toggle-buttons').click(() => Popup.ToggleTagToggleButtons('btn-stash-tag-toggle-buttons'));
	$('#btn-stash-highlight-my-likes').click(() => Popup.ToggleHighlightMyLikes('btn-stash-highlight-my-likes'));
	$('#btn-stash-apply-suggestion-text-indicator').click(() => Popup.ToggleApplySuggestionText('btn-stash-apply-suggestion-text-indicator'));
	$('#btn-stash-create-pr-description-indicator').click(() => Popup.ToggleCreatePullRequestDescription('btn-stash-create-pr-description-indicator'));
	$('#btn-stash-dashboard-load-all-pullrequests').click(() => Popup.ToggleDashboardAutoLoadAllPullRequests('btn-stash-dashboard-load-all-pullrequests'));
	// CMS
	$('#btn-cms-indicator').click(() => Popup.ToggleCmsIndicator('btn-cms-indicator'));
	$('#btn-cms-animated-indicator').click(() => Popup.ToggleCmsAnimatedIndicator('btn-cms-indicator'));

	$('#btn-options').click(() => Popup.OpenSettings());
});

// Make sure we update the button states when storage updates
AppStorage.ListenForChanges(() => Popup.InitButtonStates());